from MacOSXBridge import MacOSXBridge
from WindowXPBridge import WindowXPBridge

if __name__ == '__main__':
    mac_osx_bridge = MacOSXBridge()
    mac_osx_bridge.do_operation()
    window_xp_bridge = WindowXPBridge()
    window_xp_bridge.do_operation()
